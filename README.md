# XYPieChart for iOS like library for Android.

How to use:

1. From your xml layout file(in this case, activity_main.xml), add this view:
```xml
<!-- The rest of your layout.... -->
  <com.charts.rmpiechart.views.RMPieChart
        android:layout_width="300dp"
        android:layout_height="300dp"
        android:id="@+id/rmPie"
        android:layout_gravity="center"
        android:layout_centerInParent="true" />
<!-- The rest of your layout.... -->
```