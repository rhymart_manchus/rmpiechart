package com.charts.rmpiechart.collections;

import com.charts.rmpiechart.objects.PiePiece;

import java.util.ArrayList;

/**
 * Created by rhymart on 4/18/14.
 * RMPieChart (c)2014
 */
public class PiePieceList extends ArrayList<PiePiece> {

    private double totalValue = 0.0;

    @Override
    public boolean add(PiePiece object) {
        totalValue += object.getValue();
        return super.add(object);
    }

    public void recomputeTotalValue() {
        totalValue = 0.0;
        for(PiePiece piePiece : this) {
            totalValue += piePiece.getValue();
        }
    }

    public double getTotalValue() {
        return totalValue;
    }
}
