package com.charts.rmpiechart.objects;

/**
 * Created by rhymart on 4/21/14.
 * RMPieChart (c)2014
 */
public class PiePieceUpdate {

    private int index = 0;
    private double value = 0.0, previousValue = 0.0;

    public PiePieceUpdate() { }

    public PiePieceUpdate(int index, double value) {
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(double previousValue) {
        this.previousValue = previousValue;
    }

    public double getNewValue() {
        return previousValue+value;
    }
}
