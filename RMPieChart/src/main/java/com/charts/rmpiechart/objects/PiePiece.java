package com.charts.rmpiechart.objects;

import android.graphics.Color;

/**
 * Created by rhymart on 4/18/14.
 * RMPieChart (c)2014
 */
public class PiePiece {
    private String label = "";
    private double value = 0.0, originalValue = 0.0;
    private int color = Color.GRAY;

    public PiePiece() {}

    public PiePiece(String label, double value) {
        this.label = label;
        this.value = value;
    }

    public PiePiece(String label, double value, int color) {
        this.label = label;
        this.value = value;
        this.color = color;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getAnimatedValue() { return value; }

    public void setAnimatedValue(double value) {
        this.value = this.value+value;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public double getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(double originalValue) {
        this.originalValue = originalValue;
    }

    @Override
    public String toString() {
        return label;
    }
}
