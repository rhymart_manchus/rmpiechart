package com.charts.rmpiechart.objects;

import android.graphics.Color;

/**
 * Created by rhymart on 4/18/14.
 * RMPieChart (c)2014
 */
public class Sector {
    private double startingAngle = 0.0;
    private double endingAngle = 0.0;
    private double sweepAngle = 360.0;
    private String color = "color";
    private int sectorColor = Color.GRAY;

    public Sector() {}

    public Sector(double startingAngle, double endingAngle, String color) {
        this.startingAngle = startingAngle;
        this.endingAngle = endingAngle;
        this.color = color;
    }

    public Sector(double startingAngle, double endingAngle, int sectorColor) {
        this.startingAngle = startingAngle;
        this.endingAngle = endingAngle;
        this.sectorColor = sectorColor;
    }

    public double getStartingAngle() {
        return startingAngle;
    }

    public void setStartingAngle(double startingAngle) {
        this.startingAngle = startingAngle;
    }

    public double getEndingAngle() {
        return endingAngle;
    }

    public void setEndingAngle(double endingAngle) {
        this.endingAngle = endingAngle;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSectorColor() {
        return sectorColor;
    }

    public void setSectorColor(int sectorColor) {
        this.sectorColor = sectorColor;
    }

    public double getSweepAngle() {
        return sweepAngle;
    }

    public void setSweepAngle(double sweepAngle) {
        this.sweepAngle = sweepAngle;
    }
}
