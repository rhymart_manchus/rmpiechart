package com.charts.rmpiechart.objects;

/**
 * Created by rhymart on 4/18/14.
 * RMPieChart (c)2014
 */
public class Point {
    private double x = 0.0;
    private double y = 0.0;

    public Point() { }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public float getXFloat() {
        return (float)x;
    }

    public float getYFloat() {
        return (float)y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

}
