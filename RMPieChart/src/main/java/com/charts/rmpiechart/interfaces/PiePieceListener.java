package com.charts.rmpiechart.interfaces;

import com.charts.rmpiechart.objects.PiePiece;

/**
 * Created by rhymart on 4/18/14.
 * RMPieChart (c)2014
 */
public interface PiePieceListener {

    public void onSliceClicked(PiePiece piePiece);

}
