package com.charts.rmpiechart;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;

import com.charts.rmpiechart.collections.PiePieceList;
import com.charts.rmpiechart.interfaces.PiePieceListener;
import com.charts.rmpiechart.objects.PiePiece;
import com.charts.rmpiechart.objects.PiePieceUpdate;
import com.charts.rmpiechart.views.RMPieChart;

public class MainActivity extends Activity {

    private Button btnRed, btnBlue, btnGreen, btnOrange;
    private RMPieChart pieChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRed = (Button) findViewById(R.id.btnRed);
        btnBlue = (Button) findViewById(R.id.btnBlue);
        btnGreen = (Button) findViewById(R.id.btnGreen);
        btnOrange = (Button) findViewById(R.id.btnOrange);

        pieChart = (RMPieChart) findViewById(R.id.rmPie);
        PiePieceList piePieces = new PiePieceList();
        piePieces.add(new PiePiece("Red", 40.0, getResources().getColor(android.R.color.holo_red_light)));
        piePieces.add(new PiePiece("Blue", 40.0, getResources().getColor(android.R.color.holo_blue_dark)));
        //piePieces.add(new PiePiece("Green", 40.0, getResources().getColor(android.R.color.holo_green_light)));

        pieChart.setPiePieceList(piePieces);
        pieChart.setPiePieceListener(new PiePieceListener() {
            @Override
            public void onSliceClicked(PiePiece piePiece) {
                AlertDialog.Builder chosen = new AlertDialog.Builder(MainActivity.this);
                chosen.setMessage(piePiece.getLabel()+" slice is tapped!");
                chosen.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                });
                chosen.show();
            }
        });

        btnRed.setOnClickListener(updateRed);
        btnGreen.setOnClickListener(updateGreen);
        btnBlue.setOnClickListener(updateBlue);
        btnOrange.setOnClickListener(addValue);

    }

    private View.OnClickListener updateRed = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pieChart.updateValue(0, 10.0);
        }
    };

    private View.OnClickListener updateBlue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pieChart.updateValue(1, 50.0);
        }
    };
    private View.OnClickListener updateGreen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PiePieceUpdate ppu1 = new PiePieceUpdate(1, 50.0);
            //PiePieceUpdate ppu2 = new PiePieceUpdate(3, 50.0);
            pieChart.updateValues(ppu1);
            //pieChart.updateValue(2, 40.0);
        }
    };
    private View.OnClickListener addValue = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Multiple Adding
            // pieChart.addValue(new PiePiece("Orange", 40.0, getResources().getColor(android.R.color.holo_orange_dark)));
            PiePiece p1 = new PiePiece("Orange", 40.0, getResources().getColor(android.R.color.holo_orange_dark));
            PiePiece p2 = new PiePiece("Purple", 50.0, getResources().getColor(android.R.color.holo_purple));
            pieChart.addValues(p1, p2);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
