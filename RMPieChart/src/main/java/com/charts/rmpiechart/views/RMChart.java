package com.charts.rmpiechart.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by rhymart on 4/16/14.
 * RMPieChart (c)2014
 */
public class RMChart extends View {

    public RMChart(Context context) {
        super(context);
    }

    public RMChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RMChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
