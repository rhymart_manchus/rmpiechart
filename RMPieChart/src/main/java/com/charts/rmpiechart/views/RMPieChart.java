package com.charts.rmpiechart.views;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.opengl.GLES20;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.charts.rmpiechart.collections.PiePieceList;
import com.charts.rmpiechart.interfaces.PiePieceListener;
import com.charts.rmpiechart.objects.PiePiece;
import com.charts.rmpiechart.objects.PiePieceUpdate;
import com.charts.rmpiechart.objects.Point;
import com.charts.rmpiechart.objects.Sector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rhymart on 4/16/14.
 * RMPieChart (c)2014
 */
public class RMPieChart extends RMChart {

    private PiePieceListener piePieceListener;
    private int sectorIndex = 0;
    private List<Integer> sectorIndexes = new ArrayList<Integer>();
    private List<PiePieceUpdate> piePieceUpdates = new ArrayList<PiePieceUpdate>();
    private double startingAngle = 0.0, endingAngle = 0.0, previousValue = 0.0, newValue = 0.0;

    private float updateValue = 0.0f;
    private boolean isMultipleValues = false, isOnUpdate = false;

    private boolean isInit = true;
    private Point center = new Point();
    private float angle = 0.0f;
    private List<Sector> sectors = new ArrayList<Sector>();
    private PiePieceList piePieces = new PiePieceList();
    private RectF mOval;
    private String noDataMessage = "No data";
    private int noDataMessageColor = Color.BLACK;

    public RMPieChart(Context context) {
        super(context);
    }

    public RMPieChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RMPieChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setPiePieceList(PiePieceList piePieceList) {
        boolean updateSector = (piePieces.size() > 0); // TODO
        piePieces = piePieceList;
        sectorIndex = 0;
        updateSectors(false);
        if(isInit)
            animateInit();
        else
            invalidate();
    }

    private void updateSectors(boolean shouldInvalidate) {
        sectors.clear();
        double totalValue = piePieces.getTotalValue();
        double startingAngle = 0.0;
        for(PiePiece piePiece : piePieces) {
            Sector sector = new Sector();
            sector.setStartingAngle(startingAngle);
            sector.setColor(piePiece.getLabel());
            double sweepAngle = (piePiece.getValue() > 0.0) ? (piePiece.getValue()/totalValue)*360.0 : 0.0;
            sector.setSweepAngle(sweepAngle);
            sector.setSectorColor(piePiece.getColor());
            startingAngle += sweepAngle;
            sector.setEndingAngle(startingAngle); //  - 0.01
            sectors.add(sector);
        }
        if(shouldInvalidate)
            invalidate();
    }

    private void animateUpdatePie() {
        ValueAnimator valueAnimator = ObjectAnimator.ofFloat(this, "updateValue", 0.0f, (float)newValue);
        valueAnimator.setDuration(1000);
        AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
        valueAnimator.setInterpolator(accelerateDecelerateInterpolator);
        valueAnimator.start();
    }

    private void animateInit() {
        ValueAnimator valueAnimator = ObjectAnimator.ofFloat(this, "angle", angle, 360.0f);
        valueAnimator.setDuration(1000);
        AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
        accelerateDecelerateInterpolator.getInterpolation(0.5f);
        valueAnimator.setInterpolator(accelerateDecelerateInterpolator);
        valueAnimator.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);

        float dimensions = (getHeight() < getWidth()) ? getHeight() : getWidth();
        float radius = (getHeight()-getWidth())/2;
        mOval = new RectF(0, radius, dimensions, dimensions+radius);

        if(sectors.size() > 0) {
            if (isInit) {
                endingAngle = sectors.get(sectorIndex).getEndingAngle();
                if (angle > endingAngle) {
                    sectorIndex++;
                    if (sectorIndex == sectors.size())
                        sectorIndex = sectors.size() - 1;
                    endingAngle = sectors.get(sectorIndex).getEndingAngle();
                }
                startingAngle = 0.0;
                for (int i = 0; i < sectorIndex; i++) {
                    Sector sector = sectors.get(i);
                    startingAngle += sector.getSweepAngle();

                    paint.setColor(sector.getSectorColor());
                    canvas.drawArc(mOval, (float) sector.getStartingAngle(), (float) sector.getSweepAngle(), true, paint); // draw
                }
                paint.setColor(sectors.get(sectorIndex).getSectorColor());
                canvas.drawArc(mOval, (float) sectors.get(sectorIndex).getStartingAngle(), (angle - (float) startingAngle), true, paint); // animating sector
            } else {
                for (Sector sector : sectors) {
                    paint.setColor(sector.getSectorColor());
                    canvas.drawArc(mOval, (float) sector.getStartingAngle(), (float) sector.getSweepAngle(), true, paint);
                }
            }
        }
        else {
            paint.setColor(Color.BLACK);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(45.0f);
            canvas.drawText(noDataMessage, center.getXFloat(), center.getYFloat(), paint);
        }
        // canvas.drawCircle(getWidth()/2, getHeight()/2, (getHeight() < getWidth()) ? getHeight()/2 : getWidth()/2, paint);
        //Display disp = ((WindowManager)this.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        //float radius = 0;//Radius caused an error so I initialized this variable
        //canvas.drawCircle(disp.getWidth()/2, disp.getHeight()/2, radius, paint);
    }

    private double getAngleFromPoints(Point center, Point touch) {
        double xDiff = touch.getX() - center.getX();
        double yDiff = touch.getY() - center.getY();
        double angle = Math.toDegrees(Math.atan2(yDiff, xDiff));

        return angle < 0.0 ? 180+(180+angle) : angle;
    }

    /**
     * (X-h)^2 + (y-k)^2 =radius^2
     *
     * Acknowledgment to Ms. Sherilyn Saguirer.
     *
     * @param touch
     * @return
     */
    private boolean isInsideTheCircle(Point touch) {
        double radius = Math.pow((double)(getMeasuredWidth()/2), 2.0);
        double A = Math.pow((touch.getX()-center.getX()), 2.0);
        double B = Math.pow((touch.getY() - center.getY()), 2.0);

        return (A+B <= radius);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        center.setX(getMeasuredWidth()/2);
        center.setY(getMeasuredHeight()/2);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Point touch = new Point(event.getX(), event.getY());
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if(isInsideTheCircle(touch)) {
                double angleOfTouch = getAngleFromPoints(center, touch);
                int sectorIndex = 0;
                for(Sector sector : sectors) {
                    if(angleOfTouch >= sector.getStartingAngle() && angleOfTouch <= sector.getEndingAngle()) {
                        if(piePieceListener != null)
                            piePieceListener.onSliceClicked(piePieces.get(sectorIndex));
                        else
                            Toast.makeText(getContext(), "Sector Color: "+sector.getColor(), Toast.LENGTH_LONG).show();
                        break;
                    }
                    sectorIndex++;
                }
            }
        }
        return true;
    }

    @Override
    public boolean isInEditMode() {
        return true;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
        invalidate();
    }

    public boolean isInit() {
        return isInit;
    }

    public void setInit(boolean isInit) {
        this.isInit = isInit;
    }

    public void addValue(PiePiece piePiece) {
        isInit = false;
        isMultipleValues = false;
        previousValue = 0.0;
        newValue = piePiece.getValue();
        piePiece.setValue(0.0);
        piePieces.add(piePiece);
        sectorIndex = piePieces.size()-1;
        animateUpdatePie();
    }

    public void addValues(List<PiePiece> newPiePieces) {
        sectorIndexes.clear();
        isInit = false;
        previousValue = 0.0;
        newValue = 0.0;
        isOnUpdate = false;
        for(PiePiece piePiece : newPiePieces) {
            newValue += piePiece.getValue();
            piePiece.setOriginalValue(piePiece.getValue());

            piePiece.setValue(0.0);
            piePieces.add(piePiece);
            sectorIndexes.add(piePieces.size()-1);
        }
        isMultipleValues = true;
        animateUpdatePie();
    }

    public void addValues(PiePiece... newPiePieces) {
        addValues(Arrays.asList(newPiePieces));
    }

    public void updateValue(int index, double value) {
        isInit = false;
        isMultipleValues = false;
        sectorIndex = index;
        previousValue = piePieces.get(index).getValue();
        newValue = value;
        animateUpdatePie();
    }

    public void updateValues(PiePieceUpdate... newPiePieceUpdates) {
        updateValues(Arrays.asList(newPiePieceUpdates));
    }

    public void updateValues(List<PiePieceUpdate> newPiePieceUpdates) {
        isInit = false;
        isMultipleValues = true;
        isOnUpdate = true;
        piePieceUpdates.clear();
        newValue = 0.0;
        for(PiePieceUpdate piePiece : newPiePieceUpdates) {
            newValue += piePiece.getValue();
            piePieces.get(piePiece.getIndex()).setOriginalValue(piePieces.get(piePiece.getIndex()).getValue()+piePiece.getValue());
            piePiece.setPreviousValue(piePieces.get(piePiece.getIndex()).getValue());
            piePiece.setValue(0.0);
            piePieceUpdates.add(piePiece);
        }
        animateUpdatePie();
    }

    // TODO
    public void removePieValue() {
    }

    public void setUpdateValue(float updateValue) {
        this.updateValue = updateValue;
        if(isMultipleValues) {
            if(isOnUpdate) {
                int i = 0;
                for(PiePieceUpdate piePieceUpdate : piePieceUpdates) {
                    piePieceUpdates.get(i).setValue(updateValue);
                    if(piePieceUpdate.getNewValue() > piePieces.get(piePieceUpdate.getIndex()).getOriginalValue())
                        piePieces.get(piePieceUpdate.getIndex()).setValue(piePieces.get(piePieceUpdate.getIndex()).getOriginalValue());
                    else
                        piePieces.get(piePieceUpdate.getIndex()).setValue(piePieceUpdate.getPreviousValue() + updateValue);
                    i++;
                }
            }
            else
                for(Integer sectorIndex : sectorIndexes) {
                    double newVal = previousValue + updateValue;
                    if(newVal > piePieces.get(sectorIndex).getOriginalValue())
                        piePieces.get(sectorIndex).setValue(piePieces.get(sectorIndex).getOriginalValue());
                    else
                        piePieces.get(sectorIndex).setValue(previousValue + updateValue);
                }
        }
        else
            piePieces.get(sectorIndex).setValue(previousValue + updateValue);
        piePieces.recomputeTotalValue();
        updateSectors(true);
    }

    public float getUpdateValue() {
        return updateValue;
    }

    public PiePieceList getPiePieces() {
        return piePieces;
    }

    public PiePieceListener getPiePieceListener() {
        return piePieceListener;
    }

    public void setPiePieceListener(PiePieceListener piePieceListener) {
        this.piePieceListener = piePieceListener;
    }

    public String getNoDataMessage() {
        return noDataMessage;
    }

    public void setNoDataMessage(String noDataMessage) {
        this.noDataMessage = noDataMessage;
    }

    public int getNoDataMessageColor() {
        return noDataMessageColor;
    }

    public void setNoDataMessageColor(int noDataMessageColor) {
        this.noDataMessageColor = noDataMessageColor;
    }
}
